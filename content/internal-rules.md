Title: Estatuto Interno
Status: draft

## KAETÉ MC

Artigo 1º - Sobre o Moto Clube Kaeté (Kaeté MC).

§ 1º - Fundado em 7 de setembro de 2019, é uma associação sem fins lucrativos.
Criado no estado de Alagoas, com representatividade tanto na capital quanto no
interior. Este estatuto interno regerá as condutas dos seus filiados, fundadores,
presidente, vice-presidente e todos os demais integrantes.

§ 2º - Kaeté é um motoclube moderno (new school), com inspiração tradicional (old school)
porém adaptados à nossa realidade.

#### HIERARQUIA

Artigo 2º - Sobre a hierarquia no Kaeté MC.

§ 1º - Os escudados têm a prerrogativa de supervisionar, orientar e avaliar os
prospectos e os meios escudos. Assim como fazer valer e cumprir o estatuto
externo e interno do clube.

§ 2º - Os meios escudos tem o dever de orientar os novatos que começaram a trilhar sua
história no clube como prospecto, ajudando, orientando e esclarecendo qualquer
assunto do qual ele não possua ciência sobre o clube.

§ 3º - Os padrinhos têm o dever de supervisionar, orientar, assim como reportar nas
reuniões dos escudados, situações que possam estar ocorrendo com seu afilhado,
como por exemplo: faltas em reuniões, ausência dentro do clube, assim como suas 
condutas afim de avaliar o seu desenvolvimento.

I - Em caso de impossibilidade do padrinho estar exercendo suas funções perante
seu afilhado e ao clube, o Sgt. de Armas tem como uma de suas atribuições
averiguar a conduta do referido novato; caso o mesmo, já seja afilhado do Sgt. de Armas,
esse dever passará para Vice-presidente.

II - O Sgt. de Armas deverá cobrar do Padrinho a conduta necessária para com
seu afilhado, caso o mesmo seja o padrinho, o Vice-presidente que fará essa função,
atribuindo a advertência necessária.

§ 4º - Sempre se deve respeitar a patente superior em qualquer hipótese, tendo o
bom convívio e o respeito em primeiro lugar, principalmente em eventos oficiais do clube
ou quando estivermos recebendo visitantes.

I - Pode haver brincadeiras entre os membros, em momentos de descontração desde que o respeito seja
mútuo.

#### DOS DEPENDENTES (ATRELADOS) DE ASSOCIADOS

Artigo 3º - Poderão ser considerados dependentes o cônjuge e filhos, sendo isentos do pagamento de mensalidades.

I - Filhos se não houver capacidade civil plena.

§ 1º - Os dependentes não terão direito a voto nas Assembleias, nem às
prerrogativas exclusivas dos Escudos Fechados.

§ 2º - O dependente não poderá participar de reuniões oficiais. (exceto por
permissão unânime dos escudados presentes).

§ 3º - Os patches internos do dependente deverão ser solicitados pelo Escudado
responsável; sendo diferenciados dos membros pela ausência do patch ‘MC’ nas
costas.

I - O valor dos patches são arcados pelo membro responsável.

II - Os patches dos atrelados seguirão a graduação do membro responsável.

Artigo 4º - Quando um integrante for desligado do Kaeté MC, seus dependentes automaticamente
serão desligados; ao menos que os mesmos possuam motocicletas, e tenham interesse
em integrar-se como membro do clube, sejam aprovados pela Diretoria, passando
assim à condição de integrante, conforme termo a ser assinado no ato da inscrição,
o grau de colete será determinado em assembleia entre os Escudados.

§ 1º - Ao solicitar seu desligamento do Kaeté MC - O integrante e seus dependentes
terão que devolver à Diretoria todos os objetos de uso exclusivo dos Associados
(patch com o brasão do Kaeté MC).

§ 2º - O Kaeté MC não é obrigado a ressarcir o ex-associado ou dependentes pelos
objetos devolvidos.

Artigo 5º - Sobre conduta de dependentes:

§ 1° - Ocorrendo pelo dependente do associado conduta desabonadora, este poderá
ser advertido ou suspenso, de acordo com a gravidade da transgressão, não
refletindo no associado a punição aplicada ao dependente.

I - Em caso de conduta desabonadora, a Diretoria junto ao membro responsável,
avaliarão a responsabilização.

II - Em caso da ausência do responsável do dependente, um diretor, possuindo o
mínimo de duas testemunhas integrantes do clube, poderá aplicar a punição da
retirada do Colete.

#### DAS MENSALIDADES

Artigo 6º - O integrante que ficar com 03 meses de mensalidade atrasada
acarretará suspensão de colete, caso a dívida se acumule até o 5º mês, a dívida será
congelada tendo a tolerância até o 7º mês para regularização de todas as
mensalidades; exceto em casos que esteja em vigência o parágrafo segundo deste
artigo.

§ 1.º - A suspensão de que trata esse artigo será estendida também aos
dependentes.

§ 2.º - Em casos de suspensões de coletes, o integrante será responsável pela
entrega do colete na sede, em casos excepcionais poderá ser enviado por outro
membro; o prazo para entrega será de 15 dias a partir da notificação da suspensão.
Caso não seja cumprido o prazo, será aplicada penalidade em dobro; a contagem
regressiva do tempo de penalidade só se iniciará a partir do ato da entrega do
colete.

§ 3.º - Nos casos em que o integrante estiver desempregado ou com qualquer outro
problema que o impeça de obter renda, sua mensalidade poderá, a pedido do
integrante, ser suspensa cumulativamente ou isenta, e não existirá penalidades de
suspensão de colete por esse motivo.

§ 4.º - O integrante inadimplente não pode ascender (meio-escudo ou escudado),
salvo situações em que se aplicam o Parágrafo Segundo deste mesmo artigo.

##### DA AVALIAÇÃO DOS MEMBROS

Artigo 7º - Os membros serão avaliados pelas suas atividades, atitudes, conduta e
sintonia.

I - Para o ingresso no KAETÉ MC. o interessado tem que passar pelo crivo unânime
do conselho fundador.

II - Para progressão como meio-escudo e escudo fechado o membro seguindo os
ritos, boa conduta e proatividade dentro do clube, assim como seu progresso no
VFC será avaliado pelos escudos fechados, e passará por uma votação precisando
da aprovação unânime.

##### DO BATISMO DE ASSOCIADOS

Artigo 8º - O batismo só poderá ocorrer em duas ocasiões anuais: Aniversário
Anual e Camping de final de ano.


#### ATRIBUIÇÕES DOS CARGOS DE DIRETORIA

Artigo 9º - Sobre os cargos de diretoria:

§ 1.º - Os cargos de Presidente e Vice-Presidente serão escolhidos em reunião de fundadores.

§ 2.º - Os cargos da diretoria elegíveis a escudados, são: Secretário, Sgt. de Armas, Cap. de Estrada, Dir. Financeiro, Dir. Social, Dir. de Patrimônio e Dir. de Mídia.

§ 3.º - O Presidente da vez escolherá os cargos de Diretores sempre levando em consideração o conselho dos fundadores.

§ 4.º - Existe o recurso que possibilita a avaliação de cargos da Diretoria a cada 6 meses, onde caso o membro não estiver 
desempenhando o papel designado de forma satisfatória, qualquer Escudado poderá pedir reavaliação do cargo, no qual será decidido em reunião oficial.

#### SECRETÁRIO

Artigo 10º - Ao Secretário compete organizar toda documentação do KAETÉ MC. Montar e orientar os 
trabalhos de Secretaria, redigir, ler e assinar com o Presidente e Vice-Presidente as atas das 
reuniões administrativas em assembléias. Preparar as pautas necessárias para as reuniões e ata de reunião.
De modo geral, o Secretário deve manter atualizado todos os documentos do KAETÉ MC  como a documentação de 
registros, atualização da ficha individual e controle de frequência dos membros.

#### SGT. DE ARMAS

Artigo 11º - Ao Sgt. de Armas, especificamente, garantir que o estatuto e as regras permanentes do 
“KAETÉ MC” não sejam violados e que as ordens do Presidente, Vice-Presidente e Diretoria sejam executados
de maneira imediata, policiar e manter a ordem em todos eventos relacionados ao clube, bem como, averiguar
as faltas cometidas e reportar a diretoria para ser tomado as devidas medidas necessária. Realizar o cumprimento
de punições, recolhimento de patches e qualquer apetrecho dos membros afastado, desligados ou excluídos.

I - O Sgt. de Armas tem como uma de suas atribuições avaliar as condutas dos padrinhos e seus
referidos afilhados; em casos de afilhados do Sgt. de Armas, essa avaliação será executada pelo Vice-presidente.

II - Cabe ao Sgt. de Armas instruir os novatos sobre as regras do clube, a boa conduta e convívio social dentro e
fora do clube.

#### CAPITÃO DE ESTRADA

Artigo 12º - Ao Capitão de Estrada, especificamente, planejar roteiros e viagens e indicar principais atrativos
turísticos, coordenar, delegar funções (batedor, anjo e ferrolho), liderar os deslocamentos em comboio,
estabelecer e padronizar a comunicação e a condução das motocicletas. Proporcionando treinamento e cursos
periódicos de pilotagem e segurança, responsável pela segurança dos motociclistas e pelo bom andamento do comboio.

I - Só será permitido visitantes dentro do comboio de deslocamento, com a permissão do Capitão de Estrada,
na ausência do mesmo, a responsabilidade fica sobre o membro designado por ele.

II - Cabe ao Cap. de Estrada instruir os novatos sobre as regras da estrada.

#### DIRETOR FINANCEIRO

Artigo 13º - Ter sob sua guarda e responsabilidade toda documentação de caráter
financeiro do moto clube;

§ 1.º - Avaliar a viabilidade das compras do moto clube, monitorando compras e vendas
quando devidamente aprovadas;

§ 2.º - Assinar junto ao presidente os documentos de responsabilidade financeira;

§ 3.º - Promover a arrecadação das mensalidades;

§ 4.º - Dirigir os serviços de contabilidade e organizar o orçamento anual a ser
apresentado à diretoria; controlar movimentações contábeis e financeiras.

#### DIRETOR SOCIAL

Artigo 14º - Ter sob sua responsabilidade a organização de eventos sociais e apoio aos viajantes.

§ 1.º - Coordenar as divulgações de ações sociais e eventos do clube;

§ 2.º - Organizar os eventos sociais do clube;

§ 3.º - Organizar os apoios aos viajantes.

#### DIRETOR PATRIMÔNIO

Artigo 15º - Ter sob sua responsabilidade a organização e limpeza da sede do KAETÊ MC.

§ 1.º - Coordenar as limpezas da sede;

§ 2.º - Organizar os patrimônios do clube.

#### DIRETOR MÍDIA

Artigo 16º - Manter ativa e atualizar as mídias sociais do clube.

§ 1.º - Intermediar as comunicações por meio das mídias sociais;

§ 2.º - Divulgar e registrar o clube Kaeté MC nas mídias sociais.


#### DOS DIREITOS E DEVERES EXCLUSIVOS DOS ASSOCIADOS

Artigo 17º – Não será permitido que os Associados façam uso do nome, logotipo ou
qualquer outro material do KAETÉ MC para arrecadar benefícios pessoais, salvo
com a autorização prévia da Presidência, assim como não poderão ser utilizados
para autopromoção de qualquer Associado. Qualquer atividade que for realizada
pelos Associados utilizando o nome do KAETÉ MC deverá ter o prévio
consentimento da Presidência, caso necessário, será levado para ser decidido em
reunião da diretoria.

Artigo 18º – Cabe aos Associados relatar sempre e imediatamente ao Diretor de
conduta, ou na falta deste, a qualquer outro membro da Diretoria, os fatos ocorridos
entre os Associados e que são de interesse do KAETÉ MC, inclusive quando algum
integrante estiver portando-se de maneira inadequada, para que sejam discutidos
em reunião da Diretoria, ou levado à Assembléia.

Artigo 19º - O Associado deverá usar o colete com o brasão do KAETÉ MC sempre
que estiver em reunião oficial, em eventos oficiais do clube, ou qualquer evento do
meio motociclista, e quando estivermos dando apoio simples no domicílio da sede;
sob pena de advertência ou suspensão, salvo em casos especiais e urgentes.

§ 1.º - Fica facultado a obrigação constante deste artigo ao integrante que estiver em
suas atividades profissionais e legais.

§ 2.º - Em eventos oficiais somente é permitido o colete oficial.

§ 3.º - É permitido o uso de colete não oficial para uso cotidiano, seguindo todos os
patchs da sua graduação.

§ 4.° - Em caso de apoio completo (geralmente são apoios para dormida de um ou
mais dias) a retirada temporária do colete não gera nenhum tipo de advertência e
afins.

Artigo 20º - É dever do associado de escudo fechado, desde que esteja adimplente
com suas mensalidades, ter voto nas pautas em reuniões oficiais.

Artigo 21º - As bandeiras e colete com brasão do KAETÉ MC serão de uso exclusivo
dos Associados, não podendo ser emprestado ou usado, em hipótese alguma, por
quaisquer outras pessoas. As camisetas e acessórios com o brasão do KAETÉ MC.
poderão ser vendidas ou doadas para não associados a critério da Diretoria.

§ 1.º - Soft patches e emblemas em souvenirs com o emblema KSSK são de uso
exclusivo de escudados.

Artigo 22º - O Associado não deverá, sob pena de advertência ou suspensão a
depender da gravidade, praticar manobras arriscadas e/ou malabarismos em local
inadequado (vias públicas), colocando em risco a sua vida e a de outras pessoas.

Artigo 23º - O membro ou dependente que agredir física ou verbalmente outro
integrante, será punido com suspensão ou até mesmo expulsão do KAETÉ MC.

Artigo 24º - Todos os associados e seus dependentes terão direito de usufruir da
Sede do KAETÉ MC. e de participar dos eventos. O espaço da Sede é de convívio
social de todos os associados.

Artigo 25º - Todo integrante do KAETÉ MC tem o dever de honrar o Brasão,
respeitando não só o Estatuto, este Regimento interno como também a sociedade e
todas as leis do exercício de direito da cidadania.

Artigo 26º - Todo o integrante, enquanto estiver usando o colete do KAETÉ MC deve
obedecer às orientações dos diretores, seja na Sede, em visitas a outros MC’s, em
eventos ou onde quer que estejam, pois, os diretores são os responsáveis diretos
pela segurança e bem-estar dos associados.

Artigo 27º - Associados com Colete Fechado, Meio Escudo ou Prospecto que
saírem do KAETÉ MC. devem, em sinal de consideração, respeitar o prazo mínimo
do luto de 6 (seis) meses para ingressar em outro Motoclube ou Motogrupo.

Artigo 28º - É dever de todos os integrantes tratarem de forma respeitosa e com
zelo a motocicleta de outro integrante.

§ 1.º - Só é permitido manobrar e/ou pilotar a motocicleta de outro membro, somente
sob autorização do proprietário.

§ 2.º - Se estiver pilotando e/ou manobrando a motocicleta de outro membro, o
condutor é responsável por quaisquer danos que venham ocorrer, será seu dever
corrigir os danos causados.

Artigo 29º - Fica proibido pilotar após ingerir bebida alcoólica, exceto durante rolês
em grupo com autorização do Diretor presente.

Artigo 30º - Um membro só poderá ser penalizado se houver explicitamente escrito no Estatuto Externo
ou Interno. Salvo pautas excepcionais conforme Artigo 27º.

Artigo 31º - Votação de pautas normais exige a maioria dos votos (51%). Pautas excepcionais exige 
maioria absoluta dos votos (75%).

Artigo 32º - É proibido o uso de drogas ilícitas que vincule a imagem do Kaeté MC, seja em eventos
oficiais ou não. A vinculação do Kaeté MC com drogas ilícitas pelo membro acarretará em expulsão.

Artigo 33º - Não será penalizado o membro que usar drogas lícitas ou descriminalizadas, desde que o
membro haja com total descrição, não seja notado e não vincule a imagem do clube.

Artigo 34º - Todo membro é responsável por suas atitudes, sóbrio ou não.

#### DOS DIREITOS E DEVERES EXCLUSIVOS DOS PROSPECTOS

Artigo 35° - O Prospecto deve participar das atividades do Clube, salvo os casos em que não
seja liberado.

Artigo 36° - O Prospecto deve participar de todos os eventos internos na sede, salvo
justificativa.

Artigo 37° - O Prospecto deve seguir a hierarquia dentro do Clube:

I - Respeitar todos os membros e seus familiares.

II - Obedecer às ordens dos Escudos-Fechados.

Artigo 38° - Fica vetado o consumo de bebida alcoólica em eventos externos, salvo em caso
de permissão por parte, respectivamente, do SGT DE ARMAS, DIRETOR, PADRINHO,
ESCUDO-FECHADO (por ordem de antiguidade).

Artigo 39° - O Prospecto deve, quando solicitado, se deslocar e prestar auxílio em casos de
apoios e emergências, salvo justificativa.

Artigo 40° - O Prospecto deve, em encontros, eventos e reuniões:

I - Estar sempre acompanhado por um Escudo-Fechado.

II - Estar sempre atento para servir ao Escudo-Fechado (dentro do contexto do Clube).

III - Estar sempre atento às motos dos membros.

§ 1º - No caso de querer ir ou retornar para um evento utilizando o colete do Clube, deve
solicitar a um Escudo-Fechado para acompanhá-lo, caso contrário, fica vetado a ida ou o
retorno ao evento.

Artigo 41° - O Prospecto deve estar sempre de prontidão para quaisquer missões do Clube.

Artigo 42° - O Prospecto deverá ser um membro proativo:

I - Ao chegar na Sede, deve sempre checar: a geladeira, os banheiros, o quarto e reportar no
caso de precisar de algo.

II - Organizar e servir a comida e bebida para os presentes (em eventos/encontros).

III - Nos pontos de encontro de viagem na Sede: providenciar café da manhã.

IV - Se atentar a limpeza no local (dentro ou fora da Sede).

Artigo 43° - O Prospecto deve manter a confidencialidade quanto aos assuntos internos do
Clube.

Artigo 44° - O Prospecto deve sempre falar coisas boas a respeito do Clube.

Artigo 45° - O Prospecto deve conhecer a cultura e história do Clube.

Artigo 46° - O Prospecto não tem direito a votar nem opinar em reuniões.

I - A opinião do Prospecto será válida apenas quando solicitada ou quando for assunto aberto
a todos.

II - Em caso de sugestões, o Prospecto deve encaminhar ao seu PADRINHO ou à
DIRETORIA, para que o mesmo verifique e passe ao Clube.

Artigo 47° - O Prospecto deve providenciar comida e bebida em eventos internos (ir
comprar/buscar, após organizar com DIRETORIA ou ESCUDO-FECHADO a respeito de
pagamento ou local de retirada).


#### DOS DIREITOS E DEVERES EXCLUSIVOS DO MEIO-ESCUDO

Artigo 48° - O Meio-Escudo deve orientar e observar os Prospectos, dentro de suas
atribuições.

Artigo 49° - O Meio-Escudo deve reportar à Diretoria quaisquer infrações cometidas por
Prospectos e outros Meios-Escudos.

Artigo 50° - O Meio-Escudo deve participar das atividades do Clube, salvo os casos em que
não seja liberado.

Artigo 51° - O Meio-Escudo deve participar de todos os eventos internos na Sede, salvo
justificativa.

Artigo 52° - O Meio-Escudo deve seguir a hierarquia dentro do Clube:

I - Respeitar todos os membros e seus familiares.

II - Obedecer às ordens dos Escudos-Fechados.

Artigo 53° - Fica vetado o consumo de bebida alcoólica em eventos externos, salvo em caso
de permissão por parte, respectivamente, do SGT DE ARMAS, DIRETOR, PADRINHO,
ESCUDADO (por ordem de antiguidade).

Artigo 54° - O Meio-Escudo deve, quando solicitado, se deslocar e prestar auxílio em casos
de apoios e emergências, salvo justificativa.

Artigo 55° - O Meio-Escudo deve, em encontros, eventos e reuniões:

I - Solicitar à DIRETORIA no caso de querer ir para um evento utilizando o colete do Clube,
caso seja liberado, o Meio-Escudo poderá ir sem acompanhamento.

II - Estar sempre atento para servir ao Escudo-Fechado (dentro do contexto do Clube).

III - Policiar seu comportamento, tendo em vista que agora carrega o nome do Clube e seu
próprio nome.

IV - Coordenar os Prospectos nas atividades relacionadas a servir ao Escudo-Fechado
(comida, bebida, supervisão das motos, etc).

§ 1º - Em caso de retorno ao evento, devem solicitar, respectivamente ao PADRINHO (caso o
mesmo esteja presente no evento) e DIRETORIA.

§ 2º - Na ausência de Prospectos, o Meio-Escudo deve seguir as atribuições dos mesmos.

Artigo 56° - O Meio-Escudo deve estar sempre de prontidão para quaisquer missões do
Clube.

Artigo 57° - O Meio-Escudo deverá ser um membro proativo:

I - Ao chegar na Sede, deve sempre checar: a geladeira, os banheiros, o quarto e reportar no
caso de precisar de algo.

II - Organizar e servir a comida e bebida para os presentes (em eventos/encontros).

III - Nos pontos de encontro de viagem na Sede: providenciar café da manhã.

IV - Se atentar a limpeza no local (dentro ou fora da Sede).

Artigo 58° - O Meio-Escudo tem o dever de conhecer a cultura e história do Clube.

Artigo 59° - O Meio-Escudo tem o dever de manter a confidencialidade quanto aos assuntos
internos do Clube.

Artigo 60° - O Meio-Escudo tem o dever de sempre falar coisas boas a respeito do Clube.

Artigo 61° - O Meio-Escudo não tem direito a votar nem opinar em reuniões.

I - A opinião do Meio-Escudo será válida apenas quando solicitada ou quando for assunto
aberto a todos.

II - Em caso de sugestões, o Meio-Escudo deve encaminhar ao seu PADRINHO ou à
DIRETORIA, para que o mesmo verifique e passe ao Clube.

Artigo 62° - O Meio-Escudo deve providenciar comida e bebida em eventos internos (ir
comprar/buscar, após organizar com DIRETORIA ou ESCUDO-FECHADO a respeito de
pagamento ou local de retirada).


#### DOS DIREITOS E DEVERES EXCLUSIVOS DO ESCUDO-FECHADO.

Artigo 63° - O Escudo-Fechado deve observar e orientar os Prospectos e Meio-Escudos,
dentro de suas atribuições.

Artigo 64° - O Escudo-Fechado deve reportar ao SGT DE ARMAS quaisquer infrações
cometidas por outros Escudos-Fechados.

§ 1º - No caso dos Meios-Escudos e Prospectos, o Escudo-Fechado tem o poder de
repreender no momento da infração, mas deve mesmo assim, reportar ao SGT DE ARMAS.

§ 2º - No caso de uma infração cometida pelo SGT DE ARMAS, deve ser reportado ao
PRESIDENTE.

Artigo 65° - O Escudo-Fechado deve participar das atividades do Clube.

Artigo 66° - O Escudo-Fechado deve participar de todos os eventos internos na Sede, salvo
justificativa.

Artigo 67° - O Escudo-Fechado deve seguir a hierarquia dentro do Clube:

I - Respeitar todos os membros e seus familiares.

II - Obedecer às ordens da DIRETORIA.

Artigo 68° - O Escudo-Fechado deve, quando solicitado, se deslocar e prestar auxílio em
casos de apoios e emergências, salvo justificativa.

Artigo 69° - O Escudo-Fechado deve, em encontros, eventos e reuniões:

I - Estar sempre atento para servir à DIRETORIA (dentro do contexto do Clube).

II - Policiar seu comportamento e dar exemplo, tendo em vista que agora carrega o nome do
Clube e seu próprio nome.

III - Observar o comportamento de outros membros associados e se necessário, orientá-los.

IV - Observar e cobrar os Meios-Escudos e Prospectos nas atividades relacionadas a servir ao
Escudo-Fechado (comida, bebida, supervisão das motos, etc).

Artigo 70° - O Escudo-Fechado deve estar sempre de prontidão para quaisquer missões do
Clube.

Artigo 71° - O Escudo-Fechado deverá ser um membro proativo:

I - Ao chegar na Sede, deve sempre checar: a geladeira, os banheiros, o quarto e reportar no
caso de precisar de algo.

II - Se atentar à limpeza no local (dentro ou fora da Sede).

III - Coordenar os Meios-Escudos e Prospectos nas atividades referentes a: limpeza, comida,
bebida, supervisão das motos, etc.

§ 1º - Na ausência de Prospectos e Meios-Escudos, as atribuições passam para os
Escudos-Fechados.

Artigo 72° - O Escudo-Fechado deve conhecer a cultura e história do Clube.

Artigo 73° - O Escudo-Fechado deve manter a confidencialidade quanto aos assuntos
internos do Clube.

Artigo 74° - O Escudo-Fechado deve sempre falar coisas boas a respeito do Clube.

#### SOBRE AS REUNIÕES

Artigo 75º - As reuniões serão majoritariamente realizadas na sede, podendo
ocorrer no Agreste e Sertão (respeitando as cotas anuais já estabelecidas em
reuniões).

§ 1.º Reuniões não poderão mais ser realizadas em viagens e festas com
convidados.

§ 2.º - fica proibido o consumo de bebida alcoólica antes e durante reunião oficial.
SOBRE LICENÇAS

Artigo 76º - Tempo de licença tem período de 6 meses, podendo ser prorrogado por
mais 6 após avaliação da Diretoria;

**Parágrafo único** - O presente estatuto poderá sempre ser acrescido ou modificado a 
critério em assembléia dos escudados em reunião oficial, atualizado em 30 de Janeiro de 2025.
