Title: Honrarias
Date: 2024-9-7
Category: Motoclube
Tag: Kaeté, Desafios

Nessa página encontra-se a lista dos desafios e os respectivos membros que as concluíram.

# Caminho do Imperador

O Caminho do Imperador consiste em seguir a rota realizada pelo imperador Dom Pedro II mageando o rio São
Francisco, iniciando em Piaçabuçu até Água Branca. Sendo realizada em sua maior parte em estradas off-road.

- Junior Pinto
- Pedro Henrique
- Artner Belo
- Daniel Pimentel
- Juan Wesley
- André Alécio
- Wendson Matias

# Valente Fazedor de Chuva

O desafio Valente Fazedor de Chuva consiste em escolher entre o seu estado natal, aquele onde está vivendo 
ou ainda, à sua vontade, um estado dentro da Federação e percorrer todos os municípios que o compõe, segundo
informações oficiais do IBGE ou estadual, que será confirmado pela organização.

- Daniel Pimentel
- Edson Júnior
- Júnior Pinto
- Pedro Henrique
- José Cícero
- Djanilson Alves
- Guilherme Ferreira
- Manuel Gomes
- Artner Belo
- Caio Cesar
- Expedito Barros
- Jamio Lima
- Franklin Melo
- Wendson Matias
- Wanderson Carlos
- André Alécio
- Thiago Tenório
- Raimundo Silva
- Juan Wesley
- Ilivanilton Rezende
- Ricardo Paiva

# Rodoviário Fazedor de Chuva

O desafio Rodoviário Fazedor de Chuva, que consiste em percorrer de cabo a rabo uma das quatro rodovias que
mais atravessam estados no gigante Brasil. 

## BR-101

- Daniel Pimentel

## BR-116

- Daniel Pimentel

# Cardeal Fazedor de Chuva

Cardeal Fazedor de Chuva, o inconseqüente que fizer a cruz do Brasil, indo do Pai, representado por Chuí, Rio Grande do Sul, preferencialmente do seu Arroio, até o Filho, em Uiramutã, opcionalmente até o Monte Caburaí, finalizando estes super extremos por via fluvial ou por caminhada, e fechando o Espírito, que está localizado na Ponta do Seixas, na Paraíba, até o Santo, que fecha a cruz onde o sol se põe, em Mancio Lima, Acre, e claro, a partir daí, de novo de barco, deixando a moto descansando na cidade, até a Nascente do Rio Moa, na Serra do Divisor.

- Daniel Pimentel

# Iron Butt

A Iron Butt Association (IBA) é uma organização americana dedicada a certificar viagens de longa distância de motos, com mais de 75 mil membros.

Saddlesore1600K é um desafio que consiste em percorrer 1.600 km em menos de 24 horas.

Mile Eater reconhece os membros que completam vários desafios certificados. 

Not Right Riders reconhece o piloto que executa um dos desafios com uma moto de mais de 40 anos ou com uma moto abaixo de 250cc.

## Saddlesore 1600K

- Daniel Pimentel
- Manuel Gomes
- Junior Pinto
- Myrella Pinto

## Mile Eater

- Daniel Pimentel
- Junior Pinto

## Not Right Riders

- Junior Pinto
