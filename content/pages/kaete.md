Title: Kaeté
Date: 2019-9-7
Category: Motoclube
Tag: Kaeté

**Kaeté Moto Clube** é um moto clube brasileiro fundado em 2019 no estado de
**Alagoas**. 

# Nome

Kaeté, nome de origem tupi, foi o povo indígena brasileiro de língua tupi 
antiga oriundo do estado de Alagoas. Este povo habitou o litoral do Brasil 
entre a ilha de Itamaracá e o rio São Francisco no século XVI. Os Kaeté eram 
cerca de 75 mil indivíduos. Em liberdade, os kaetés eram exímios pescadores e 
caçadores que faziam seus próprios instrumentos. 

O nome Kaeté foi definido pelos seus fundadores, inspirados pelo regionalismo 
Alagoano, que viram nos índios nativos (Kaeté) de Alagoas a força e o 
sentimento de liberdade para a condução de suas motocicletas sempre prezando o
espírito de companherismo nas aventuras.

# Membros

Atualmente o Kaeté Moto Clube é formado pelos membros a seguir.

## Fundadores

- [Daniel Pimentel (B+)](https://d4n1.org);
- Edson Júnior (A+);
- Júnior Pinto (O+);
- Pedro Henrique (O+).

## Padrinho

- José Cícero (A+).

## Escudo-fechado

- Djanilson Alves (O+);
- Guilherme Ferreira (O-);
- Manuel Gomes (O+);
- Artner Belo (O+);
- Caio Cesar (A+);
- Ramon Santos (A+);
- Expedito Barros (O-);
- Jamio Lima (A+);
- [Franklin Melo (A+)](https://instagram.com/franklin_melo);
- Wendson Matias (B+);
- Wanderson Carlos (O+).
- André Alécio (A+);
- Thiago Tenório (O+);
- Juan Wesley (Juan) A+.

## Meio-escudo

- Pedro Matias (O+);
- Ilivanilton Rezende (A+);
- Raimundo Silva (AB+);

## Prospectos

- Ricardo Paiva (O+);
- Kemoel Lima (B+)
- Carlos Edoardo (O+)

# Objetivos

O objetivo e filosofia principais dos integrantes do Kaeté Moto Club são: 

- viagens e passeios com suas motocicletas;
- participação em eventos motociclísticos;
- confraternização entre seus Integrantes e demais grupos de motociclistas; 
- lazer e entretenimento saudáveis.

# Quem somos?

O **Kaeté Moto Clube** não é uma entidade filantrópica, tampouco com fins 
lucrativos.

# Logo

Nossa logomarca é constituida de:

- uma caveira que representa à todos sem diferença de raça, sexo, cor ou credo;
- um cocar indigena que representa através de suas penas os 102 municípios de
  Alagoas.

A fonte usada na logo é de origem indígena.

# Escudo

![Kaeté Moto Club](/theme/images/patch.svg.png)

# Apologia

O **Kaeté Moto Clube** não compactua com qualquer alusão a apologia ao crime, 
discriminação de raça, sexo, cor ou credo, bem como não apresenta 
vínculos políticos e religiosos, com nenhuma entidade; além disso, não é 
responsável direta ou indiretamente, pela conduta pessoal de seus Integrantes, 
que contrariem as leis vigentes, os quais responderão, civil e criminalmente 
por si, sempre que se fizer necessário.

# Ingresso

O **Kaeté Moto Clube** aceita o ingresso de pessoas que devem ser convidadas e 
apadrinhadas por um membro Kaeté.
O futuro ingressante deve possuir a devida habilitação para condução 
de sua motocicleta, rodar com o **Kaeté Moto Clube** e participar de algumas
atividade. Para mais detalhes acesse o nosso
**[Estatuto](http://kaetemoto.club/pages/estatuto.html)**.
