Title: Estatuto

## KAETÉ MC

O Moto Clube Kaeté – AL, fundado em 7 de setembro de 2019 é uma associação sem fins lucrativos. Criado no estado de Alagoas, com representatividade tanto na capital quanto no interior.

Este estatuto regerá as condutas dos seus filiados, fundadores, presidente, vice presidente e todos os demais integrantes. Podendo abrir filiais em outros estados.

O Kaeté é um moto clube forjado no coração do nordeste, entre as belezas naturais de seu litoral e o calor do sertão, tendo passado em todas as cidades de Alagoas, todos os fundadores e membros de escudo fechado, obrigatoriamente, conhecem o estado de ponta a ponta.


#### DO CLUBE

**Artigo 1°** - O Kaeté MC é uma entidade associativa constituída juridicamente, sem fins lucrativos, de facções
partidárias ou religiosas. Tem como objetivos sociais:

**§ 1°** - Organizar passeios, reuniões, viagens e eventos para seus membros, famílias, amigos e visitantes.

**§ 2°** - Incentivar o uso correto da motocicleta de acordo com as leis de trânsito, assim como, estimular o espírito motociclístico de modo geral.

**§ 3°** - Prezar pela boa convivência e camaradagem entre grupos afins, não importando: brasão, estilo, marca ou cilindrada.

**§ 4°** - Desempenhar atividades de caráter beneficente e de assistência social uma vez por ano, no mínimo.

**§ 5°** - Comprometimento com as raízes do motociclismo: fraternidade, companheirismo, honestidade e respeito.

#### DOS ASSOCIADOS 

**Artigo 2°** - São considerados membros do Kaeté MC pessoas físicas que tenham sido previamente aprovadas pelos conselho de fundadores e se enquadram nas normas do estatuto seguindo a hierarquia: 

**§ 1°** - São fundadores, exclusivamente, os que participaram do ato de fundação do Moto Clube e subscreveram o Estatuto. Eles fazem parte da diretoria, também se enquadram como membros efetivos. 

**§ 2°** - Os membros de escudo fechado são associados com escudo fechado.

**§ 3°** - Os meios escudos são associados em avaliação que usam metade dos patchs do Kaeté Mc.

**§ 4°** - Os prospectos são associados em avaliação que usam identificação do Kaeté Mc e frequentam o círculo de membros efetivos. 

**§ 5°** - Os honorários são aqueles a quem o brasão for concedido com pleno consenso dos fundadores como homenagem por serviços prestados ao Kaeté MC, ao motociclismo ou a sociedade. 

**§ 6°** - Os atrelados são: esposas, esposos e filhos de membros escudo fechado. Podendo estes, utilizar o brasão do moto clube. Os membros de escudo fechado respondem pelos quaisquer atos de seus atrelados.

**Artigo 3°** - O motoqueiro pode ter acesso ao moto clube como visitante através de indicação de um membro, bem como, que de alguma forma tenha tomado conhecimento e por iniciativa própria entrou em contato com os Kaeté. Diante disto para ser admitido como prospecto é necessário cumprir com as seguintes condições:

**§ 1°** - Aceitar e submeter-se ao Estatuto do Kaeté MC e demais normas, declarando assim, identificar-se com o objetivo social da entidade.

**§ 2°** - Ter habilitação para condução de motocicletas.

**§ 3°** - Ser motociclista.

**§ 4°** - Apresentar nada consta estadual e federal.

**§ 5°** - Passar declaração que desobriga o Kaeté MC de quaisquer responsabilidades por seus atos.

**§ 6°** - Frequentar o meio social do clube como visitante e rodar com os membros algumas vezes.

**§ 7°** - Ter aprovação do crivo unânime do conselho.

**§ 8°** - O padrinho será definido pelo conselho, sendo esse um membro de escudo fechado. Cada padrinho pode ter no máximo dois afilhados por vez (prospecto e/ou meio escudo) e será como conselheiro para ele.

**Artigo 4°** - São condições de ascensão no Kaeté MC:

**§ 1°** - O visitante passa a ser prospecto, por critérios de conduta e rito de acordo o Artigo 3.

**§ 2°** - O prospecto passa a ser membro de meio escudo, por critérios de conduta e rito.

**§ 3°** - O membro de meio escudo da matriz “Alagoas”, passa a ser membro de escudo fechado, por critérios de conduta e rito. Assim como, obrigatoriamente ter passado por todos os municípios de Alagoas, registrando sua passagem com sua moto em cada prefeitura desses municípios. Como também, tempo mínimo de seis meses na categoria meio escudo e um ano dentro do moto clube.

**§ 4°** - O membro de meio escudo da Subsede passa a ser membro de escudo fechado, por critérios de conduta e rito. Assim como, obrigatoriamente ter passado pelos 102 municípios do seu estado, registrando sua passagem com sua moto em cada prefeitura desses municípios. Como também, tempo mínimo de seis meses na categoria meio escudo e um ano dentro do moto clube.

**§ 5°** - Membros honorários são regidos pelo Artigo 2°.
 
#### DOS DIREITOS E DEVERES 

**Artigo 5°** - Os Fundadores tem direito de:

**§ 1°** - Votar para cargos de presidente e vice.

**§ 2°** - Votar alterações no estatuto.

**§ 3°** - Votar para entrada de novo associado.

**§ 4°** - Renunciar ou se abster de mandatos.

**§ 5°** - Todo membro de escudo fechado tem direito: 
  
- **I** - usar o escudo do moto clube;
- **II** - votar sobre assuntos colocados em pauta pelo conselho/diretoria;
- **III** - tirar licença por motivos de interesse particular, por tempo indeterminado. Desde que, durante o período de licença, não ingresse em nenhum outro moto clube; 
- **IV** - o membro que estiver afastado, por motivo de licença de interesse particular, pode frequentar festas ou eventos do clube. Desde que haja comunicação prévia à diretoria, assim como pagamento de uma taxa corrigida referente às despesas do mesmo;
-  **V** - participar de todos os eventos do moto clube;
- **VI** - examinar a qualquer tempo e sem prévio aviso os livros, balancetes, balanços, contas, documentos, bem como solicitar informações de natureza econômico-financeira e patrimonial;
- **VII** - apresentar-se com suas(seus) acompanhantes ou companheiras(os) ou convidadas(os);
- **VIII** - renunciar ou se abster a cargos;
- **IX** - desligar-se da associação;
- **X** - ser tratado de forma digna, sem distinções.

**§ 6°** - Dos meios escudos e prospectos:

- **I** - usar patchs de sua graduação;
- **II** - participar de todos eventos do motoclube;
- **III** - ser tratado de forma digna, sem discriminação.

**§ 7°** - Dos prospectos:

- **I** - usar patchs de sua graduação;
- **II** - participar de todos eventos do motoclube;
- **III** - ser tratado de forma digna, sem discriminação.

**Artigo 6°** - São deveres dos associados: 

**§ 1°** - Cumprir todas as disposições deste Estatuto.

**§ 2°** - Comparecer às reuniões, que serão feitas mensalmente, de forma oficial. Não implicando em reuniões informais no intervalo entre uma reunião e outra.

**§ 3°** - Usar o escudo do Kaeté MC sempre que possível no dia a dia, sendo obrigatório o uso em: viagens, encontros, reuniões e qualquer evento que envolva o motociclismo, desempenhando, ainda, fielmente as funções para a qual tenha sido indicado.

**§ 4°** - Prestar ajuda aos demais membros do clube, sempre que possível.

**§ 5°** - Não faltar mais de 1/3 das reuniões do clube, assim como eventos confirmados previamente pelo moto clube.

**§ 5°** - O pagamento de mensalidade é obrigatório, regido pelo Artigo 12.

**§ 6°** - Não externar assuntos internos do clube.

**§ 7°** - Compromisso com o Kaeté MC, consideramos a hierarquia de prioridades: Família, trabalho e moto clube.

**§ 8°** - Zelar pela preservação do patrimônio moral e material, pelos bons costumes, entre associados ou não, eximindo-se de quaisquer práticas que possa denegrir a imagem e o bom nome do moto clube e os seus associados.

**§ 9°** - Responsabilizar-se por atos, atitudes, comportamento ou danos praticados por si, suas(seus) acompanhantes ou convidadas(os), desonerando sempre o moto clube.
 
**§ 10°** - Caso o membro seja desligado do clube, é obrigatória a devolução do escudo do clube, assim como qualquer outra peça de identificação do clube.

**§ 11°** - O voto de Minerva é definido pela maioria dos votos dos Fundadores,
caso ocorra empate logo o voto do presidente será o decissório.

**Artigo 7°** - Extingue-se a condição de associado:

**§ 1°** - Por solicitação espontânea do próprio.

**§ 2°** - Por aplicação da penalidade de exclusão, na forma do Estatuto.

**§ 3°** - Por morte.
 
**§ 4°** - Na hipótese do inciso II caberá recurso com efeito suspensivo à primeira assembléia geral que ocorrer após o fato. 
 
#### DAS PENALIDADES

**Artigo 8°** - Serão aplicáveis as seguintes penalidades:

**§ 1°** - Advertência disciplinar, por:

- **I** - Desrespeito ou desacato para com qualquer um dos integrantes do moto clube;
- **II** - Promover ou participar de quaisquer atos que atentam contra a ordem legal e com os interesses do moto clube, ou não utilizar com galhardia as cores e indicativos do Kaeté;
- **III** - Qualquer ato de desrespeito do membro para com sua própria família, já que baseamos o Kaeté MC na integração familiar dos membros, formando uma grande família.
  
**§ 2°** - Suspensão, por:

- **I** - Reincidência da penalidade prevista no inciso anterior;
- **II** - Inobservância de quaisquer um dos dispositivos previstos nos atos normativos do moto clube;
- **III** - O membro acusado por crimes dolosos e afins, enquanto durar o processo criminal;
- **IV** - Qualquer postura ou ação que demonstre falta de respeito à família de um membro, visto que, tratamos todos de forma igual, independente de orientação sexual, tipo familiar e afins.
  
**§ 3°** - Expulsão, por:

- **I** - Promover ou participar, direta ou indiretamente, de atos atentatórios à dignidade social, do motociclismo ou da vida em irmandade;
- **II** - Incapacidade civil não suprida;
- **III** - Quaisquer outros motivos devidamente fundamentados e por requerimento formulado por quaisquer associado, se aprovado por dois terços dos votos de escudo fechado presentes em Assembléia Geral.
- **IV** - Sentença judicial com trânsito em julgado, em casos de crimes dolosos e afins, acarretará em desligamento do membro.

**§ 4°** - As penalidades poderão ser aplicadas por quaisquer dos diretores, vedada a cumulatividade entre eles e
executadas mediante ofício pela Diretoria.

**§ 5°** - Na hipótese do inciso II, o diretor que aplicar a penalidade deverá fixar o prazo da punição.

**§ 6°** - No ato da suspensão o associado tem que entregar seu colete ao diretor que só será devolvido ao término da suspensão.
 
#### DO BRASÃO E COLETES DO KAETE MC

**Artigo 9°** - Os símbolos e organização quanto às vestes do Kaeté MC são:

**§ 1°** - O Brasão, que consiste em uma caveira, com penas formando um cocar.

**§ 2°** - As cores oficiais são Preto e Branco.

**§ 3°** - Coletes em couro, apenas com o brasão do Kaeté MC no peito do lado esquerdo. O lado direito do peito está reservado para a identificação do membro e seu tipo sanguíneo, assim como identificação interna do clube:

- **I** - Associados de colete fechado são distinguidos pelo patch com o nome KAETÉ MC, seguido da caveira símbolo e o patch do estado do associado;
- **II** - Associados de meio escudo são distinguidos pelo patch com o nome KAETÉ MC e o patch do estado do associado;
- **III** - Associados prospectos são distinguidos pelas costas lisas;

**§ 4°** - Patchs, insígnias, emblemas e afins, apenas serão autorizados se forem oriundos de mérito, desafios ou honrarias. 
 
#### DAS COMPETÊNCIAS, PATRIMÔNIO E ORGANIZAÇÃO ADMINISTRATIVA

**Artigo 10°** - Compete ao presidente: 

**§ 1°** - Representar o moto clube Juridicamente;

**§ 2°** - Representar o moto clube em reuniões e eventos externos;

**§ 3°** - Presidir as reuniões e assembleias;

**§ 4°** - Representar o moto clube junto a empresas públicas e privadas, entidades, moto clubes, etc;

**§ 5°** - Assinar, juntamente com um Diretor, qualquer contrato referente ao item acima;

**§ 6°** - Tomar decisões em conjunto com o conselho de fundadores.
 
**Artigo 11°** - Compete ao vice-presidente:

**§ 1°** - Na ausência do Presidente, praticar qualquer ato de sua competência;

**§ 2°** - Convocar assembleias gerais, ordinárias e/ou extraordinárias;

**§ 3°** - Auxiliar o presidente em todos os atos de sua competência.

**Artigo 12°** - Compete aos fundadores: 

**§ 1°** - Serem elegíveis aos cargos de: presidente, vice-presidente e demais cargos do moto clube;

**§ 2°** - Trabalhar em conjunto com o presidente e vice-presidente, constituindo um crivo decisivo;

**§ 3°** - Dar suporte a todos os membros do clube, sem distinção alguma.

**Artigo 13°** - Compete ao Secretário:

**Parágrafo único** - Elaborar e executar o processo seletivo do moto clube, criar as ferramentas que propiciem a melhoria contínua nas condições dos integrantes, avaliar o desempenho dos prospectos, elaborar atas das reuniões administrativas e assembleias, encaminhamento de e-mail, controle de ficha individual.
 
**Artigo 14°** - Compete ao Diretor Financeiro:

**§ 1°** - Ter sob sua guarda e responsabilidade toda documentação de caráter financeiro do moto clube;

**§ 2°** - Efetuar as despesas do moto clube, monitorando compras e vendas quando devidamente aprovado em assembleia;

**§ 3°** - Assinar junto com presidente os cheques e demais documentos de responsabilidade financeira;

**§ 4°** - Promover a arrecadação das mensalidades;

**§ 5°** - Dirige os serviços de contabilidade e organiza o orçamento anual a ser apresentado a diretoria, controla movimentações contábeis e financeiras.

**Artigo 15°** - Compete ao Diretor de Disciplina:

**Parágrafo único** - Caberá ao Diretor de Disciplina comunicar aos conselheiros e a diretoria, todas as faltas que os membros
  associados cometerem, no seio da associação ou fora desta, para que possa ser instaurado o procedimento
  administrativo, facultando ao interessado todos os meios de defesas pertinentes, sempre observando o Princípio
  da Ampla Defesa e do Contraditório, a fim de julgar cada caso concreto. Caberá ainda ao Diretor de Disciplina,
  em conjunto com conselho, após a apresentação de todas as provas acostadas ao procedimento administrativo julgar
  todos os atos, infrações ou faltas praticados pelos membros associados.
 
**Artigo 16°** - Compete ao Capitão de Estrada:

**§ 1°** - Definir e coordenar formação de passeio de múltipla moto;

**§ 2°** - Delegar funções no comboio;

**§ 3°** - Verificar a segurança dos pilotos;

**§ 4°** - Planejar roteiros e viagens e mesmo servir como guia de turismo.
 
**Artigo 17°** - Disposições sobre as mensalidades: 

**§ 1°** - Todos associados, fundadores, presidente e vice-presidente tem o dever de pagar as mensalidades;

**§ 2°** Membros honorários e atrelados não pagam;

**§ 3°** - O valor da mensalidade é determinado na assembleia geral;

**§ 4°** - A quantia arrecadada será utilizada para confecção de adesivos, bandeiras, ações sociais e demais necessidades do Kaeté MC, dependendo do bom senso e deliberado em assembleia ou reuniões extraordinárias;

**§ 5°** - A prestação de contas será irrestrita para os membros, exposta nas reuniões oficiais, assim como em demais canais de comunicação;

**§ 6°** - O membro não poderá acumular 3 meses sem pagamento;

**§ 7°** - Há a figura do tesoureiro, responsável pela organização das finanças do clube.
 
**Artigo 18°** - Constitui patrimônio do clube:

**§ 1°** - Brasões e quaisquer identificações são de propriedade do Kaeté MC, devendo o membro entregar à diretoria em caso de desligamento; 

**§ 2°** - Bandeira e demais objetos de representação do clube. 

#### DA SEDE

**Artigo 19°** Sobre a sede:

**§ 1°** - Aviso prévio do uso da sede;

**§ 2°** - Chave da sede somente para escudos fechados;

**§ 3°** - Uso restrito apenas nas áreas externas;

**§ 4°** - Limpeza semanal de acordo com escala previamente definida;

**§ 5°** - Proibido o uso para fins externos sem a devida autorização.

#### DAS SUBSEDES

**Artigo 20°** - Sobre subsedes e membros de outros estados:

**§ 1°** - Subsedes só serão criadas caso o estado tenha pelo menos 1 (um) membro de escudo fechado, 1 (um) meio escudo e pelo menos 4 (quatro) membros. Sendo delegado a diretoria de seu estado por crivo unânime dos fundadores;

**§ 2°** - Os membros das subsedes votarão a inclusão e exclusão de futuros membros que estão atrelados a sua subsede.

**Artigo 21** - Dos visitantes e suporte

**§ 1°** - Será permitido aos visitantes acompanharem o Kaeté MC. nos eventos sem nenhum comprometimento de ambas as partes, desde que respeitem as regras de condução da motocicleta e de comportamento nos eventos.

**§ 2°** - Só será permitido visitantes dentro do comboio de deslocamento, com a permissão do Capitão de Estrada e que na ausência do mesmo o Presidente.

**§ 3°** - Serão considerados Suportes aqueles que mostrarem interesse pelas atividades e participar esporadicamente de alguns eventos  do Kaeté MC.

**§ 4°** - Os suportes e visitantes não poderão participar de reuniões oficiais. (exceto por unanimidade da permissão dos Escudados presentes permitindo o acesso do mesmo).

**§ 5°** - Poderá ingressar como membro do Kaeté MC., devendo, para ingresso, cumprir todas as exigências deste regulamento.

**Artigo 22°** - Sobre as modificações neste estatuto:

**Parágrafo único** - O presente estatuto poderá sempre ser acrescido ou modificado a critério de comissão formada pelos fundadores, atualizado em 17 de setembro de 2023.
