#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Kaeté Moto Clube'
SITENAME = 'Kaeté Moto Clube'
SITEURL = ''
COPYRIGHT='Kaeté Moto Clube'
SITESUBTITLES = ('', '')

PATH = 'content'

TIMEZONE = 'America/Maceio'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (
    ('Kaeté Moto Clube', 'https://kaetemoto.club/'),
)

# Social widget
SOCIAL = (
    ('Telegram', '#'),
    ('Twitter', 'https://twitter.com/kaetemc'),
    ('Instagram', 'https://www.instagram.com/kaetemc'),
)

DEFAULT_PAGINATION = 8

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

THEME = 'theme'

PLUGINS = ['ical', 'i18n_subsites', ]
JINJA_ENVIRONMENT = {
    'extensions': ['jinja2.ext.i18n'],
}
